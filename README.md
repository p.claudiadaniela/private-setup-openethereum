# private-setup-openethereum


## Clone Project


```
git clone https://gitlab.com/p.claudiadaniela/private-setup-openethereum.git
cd private-setup-openethereum

```

## Download OpenEthereum

You can download the OpenEthereum client from the official Github repository, by going to the releases section https://github.com/openethereum/openethereum/releases.
Please note that several other options are available, including building the application directly from the source code.
Copy the executable openethereum file in the private-setup-openethereum folder, or add it on PATH to be accesible from cmd.

(* The current configuration files have been tested against OpenEthereum – version v3.3.0-rc.6-stable)


## Start OpenEthereum PoA Node
Start the OpenEthereum node considering the private setup configured through the config-dev.toml and chainspec.json files.

```
openethereum --config config.dev.toml

```
